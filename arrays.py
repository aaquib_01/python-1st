'''#1)WAP to read an array of 10 numbers and find their sum

list1 =[1,2,3,4,5,6,7,8,9,10]
result=sum(list1)
print("sum of the array is ",result

#2)WAP to read an array of 10 numbers and count all EVEN and ODD number

list1 =[1,2,3,4,5,6,7,8,9,10]
even_count, odd_count = 0, 0
for num in list1:

    if num % 2 == 0:
        even_count += 1
  
    else:
        odd_count += 1
        
print("Even numbers in the list: ", even_count)
print("Odd numbers in the list: ", odd_count)

#3)WAP to read an array of 10 numbers and find greatest of them

list1 = [106,88, 250,1,5,99,44, 84, 25, 111]
print("Largest greatest is:", max(list1))

#4)WAP to read an array of 10 numbers and sort it in ascending order
list = [1, 3, 4, 2,5,8,9,0,7,6]
print("before sorting",list)
list.sort()
print("after sorting",list)
'''


